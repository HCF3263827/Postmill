import './alerts';
import './comment-count';
import './commenting';
import './delete';
import './dropdowns';
import './fetch_titles';
import './forms';
import './markdown';
import './relative-time';
import './remaining';
import './select2';
import './subscribe';
import './syntax';
import './vote';
